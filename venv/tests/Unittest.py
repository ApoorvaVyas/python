import unittest
from Calculator import Cal

class Sampletest(unittest.TestCase):

    def test(self):
        self.assertTrue(1 == 1)

    def test1(self):
        self.assertTrue(2 == 2)

    @classmethod
    def setUpClass(cls):
        print("Class set up begin")

    @classmethod
    def tearDownClass(cls):
        print("Class Teardown completed")

    def setUp(self):
        self.c = Cal()
        print("Begin Set up")

    def tearDown(self):
        print("Completed")

    def test_add(self):
        d = self.c.add(5, 8)
        self.assertEqual(d, 13)

    def test_sub(self):
        d = self.c.sub(8, 5)
        self.assertEqual(d, 3)
unittest.main()

class Cal():
    def add(self, a, b ):
        return (a + b)

    def sub(self, a, b):
        if a < b:
            #raise Exception("substraction by greater number not allowed")
            raise ABCException("substraction by greater number not allowed")
        return (a - b)

    def mul(self, a, b):
        return (a * b)

    def div(self, a, b):
        if b < 0:
            raise Exception("division by negative is not allowed")
        return (a / b)

class ABCException(Exception):
    def __int__(self, a):
        super.__init__(a)
